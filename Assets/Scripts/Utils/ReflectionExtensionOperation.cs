﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Utils
{
    public static class ReflectionExtensionOperation
    {
        public static PropertyInfo GetPropertyInfo<S, T>(this Expression<Func<S, T>> propertySelector)
        {
            var body = propertySelector.Body as MemberExpression;
            if (body == null)
                throw new MissingMemberException("something went wrong");

            return body.Member as PropertyInfo;
        }

    }
}
