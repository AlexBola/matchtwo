﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.DataBinding;
using UnityEngine;

namespace Assets.Scripts.MVCBased.GameFlow
{
    public class GameFlow : AModel
    {
        private AData<string> _name;
        private AData<string> _playerName;
        private AData<GameState> _gameState;

        protected override void Initialize()
        {
            base.Initialize();
            _name = new AData<string>("MatchTwo");
            _playerName = new AData<string>("Alexander");
            _gameState = new AData<GameState>(GameState.Menu);
    }

        [BindableAttrebute]
        public AData<string> Name
        {
            get => _name;
        }

        [BindableAttrebute]
        public AData<string> PlayerName
        {
            get => _playerName;
        }

        [BindableAttrebute]
        public AData<GameState> State
        {
            get => _gameState;
        }

        public void ReplaceName(String value)
        {
            _name.Value = value;
            PropertyChanged(_name);
        }

        public void ReplacePlayerName(String value)
        {
            _playerName.Value = value;
            PropertyChanged(_playerName);
        }

        public void ReplaceState(GameState newState)
        {
            _gameState.Value = newState;
            //Debug.Log($"GameState: {State.GetHashCode()}");
            PropertyChanged(_gameState);
        }
    }
}