﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Assets.Scripts.DataBinding
{
    [System.AttributeUsage(System.AttributeTargets.Property)]  
    public class BindableAttrebute : System.Attribute  
    {  
    }  

    public abstract class AModel
    {
        private readonly Dictionary<int, List<Subscription>> _subscriptions
            = new Dictionary<int, List<Subscription>>();

        protected AModel()
        {
            Initialize();
            InitSubscriptions();
        }

        protected virtual void Initialize()
        {
        }

        private void InitSubscriptions()
        {
            foreach (PropertyInfo propertyInfo in this.GetType().GetProperties().
                Where(p => p.GetCustomAttributes(typeof(BindableAttrebute)).Any()))
            {
                if (propertyInfo.GetValue(this) == null)
                {
                    throw new Exception("$AModel Initialize error: Bindable property {propertyInfo.Name} should be inited!");
                }
                _subscriptions.Add(propertyInfo.GetValue(this).GetHashCode(), new List<Subscription>());
            }
        }

        protected void PropertyChanged<TData>(TData data)
            where TData : IBindableData
        {
            var hash = data.GetHashCode();
            if (!_subscriptions.ContainsKey(hash))
                return;
            var list = _subscriptions[hash];
            for (var i = 0; i < list.Count; i++)
            {
                list[i].Execute.Invoke();
            }
        }

        public void Subscribe<TData>(TData matcher, Func<TData, bool> filter, Action<TData> handler)
            where TData : class, IBindableData
        {
            var hash = matcher.GetHashCode();
            if (!_subscriptions.ContainsKey(hash))
            {
                throw new Exception($"AModel Subscription error: {matcher} isn't property of {this}!");
            }
            var subHash = hash + handler.GetHashCode();
            Subscription subscription = new Subscription(
                subHash,
                () =>
                {
                    if (filter(matcher))
                    {
                        handler.Invoke(matcher);
                    }
                });
            _subscriptions[hash].Add(subscription);
        }

        public void Unsubscribe<TData>(TData data, Action<TData> handler) where TData : class, IBindableData
        {
            var hash = data.GetHashCode();
            if (!_subscriptions.ContainsKey(hash))
            {
                throw new Exception($"AModel Unsubscrube error: {data} isn't property of {this}!");
            }
            var subHash = hash + handler.GetHashCode();
            var list = _subscriptions[hash];
            foreach (var subsription in list)
            {
                if (subsription.Hash != subHash)
                {
                    continue;
                }
                list.Remove(subsription);
                subsription.Dispose();
                break;
            }
        }
    }

    public class Subscription : IDisposable
    {
        public int Hash { private set; get; }
        public Action Execute { private set; get; }

        public Subscription(int hash, Action execution)
        {
            Hash = hash;
            Execute = execution;
        }

        public void Dispose()
        {
            Execute = null;
            Hash = 0;
        }
    }
}
