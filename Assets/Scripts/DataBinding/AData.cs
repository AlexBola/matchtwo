﻿using Assets.Scripts.DataBinding;

public class AData<T> : IBindableData
{
    public T Value { set; get; }

    private int _hash;

    public AData(T value)
    {
        Value = value;
        _hash = GetHashCode();
    }

    public override int GetHashCode()
    {
        if (_hash == 0)
            _hash = base.GetHashCode();
        return _hash;
    }
}
