﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.MVCBased.GameFlow;
using UnityEngine;
using UnityEngine.UI;

public class AppStarter : MonoBehaviour
{
    private GameFlow gameFlow;
    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    private void Init()
    {
        gameFlow = new GameFlow();
        Debug.Log("Subscribe");
        gameFlow.Subscribe(gameFlow.Name, data => true, OnGameNameUpdate);
        gameFlow.Subscribe(gameFlow.PlayerName, data => true, data => { Debug.Log($"Player Name: {data.Value}"); });
        gameFlow.Subscribe(gameFlow.State, data => true, data => { Debug.Log($"Game State: {data.Value}"); });

        Debug.Log("-=Replace Fields=-");
        gameFlow.ReplaceName("Jagernaut");
        gameFlow.ReplacePlayerName("Pterodactyl");
        gameFlow.ReplaceState(GameState.Gameplay);

        Debug.Log("Unsubscribe");
        gameFlow.Unsubscribe(gameFlow.Name, OnGameNameUpdate);

        Debug.Log("-=Replace Fields=-");
        gameFlow.ReplaceName("Stylo");
        gameFlow.ReplacePlayerName("Godzilla");
    }

    private void OnGameNameUpdate(AData<string> data)
    {
        Debug.Log($"Game Name: {data.Value}");
    }   
}
